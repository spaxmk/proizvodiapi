﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProizvodiAPI.Interfaces;
using Moq;
using ProizvodiAPI.Controllers;
using ProizvodiAPI.Models;
using System.Web.Http;
using System.Web.Http.Results;
using System.Linq;

namespace ProizvodiAPI.Tests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetReturnsKategorijaWithSameId()
        {
            //Arrange
            var mockRepository = new Mock<IKategorijaRepository>();
            mockRepository.Setup(x => x.GetById(10)).Returns(new Kategorija { Id = 10 });
            var controller = new KategorijeController(mockRepository.Object);

            //Act
            IHttpActionResult actionResult = controller.GetById(10);
            var contentResult = actionResult as OkNegotiatedContentResult<Kategorija>;

            //Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(10, contentResult.Content.Id);

        }

        [TestMethod]
        public void GetReturnsNotFound()
        {
            // Arrange
            var mockRepository = new Mock<IKategorijaRepository>();
            var controller = new KategorijeController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetById(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            // Arrange
            List<Kategorija> kategorijas = new List<Kategorija>();
            kategorijas.Add(new Kategorija { Id = 1, Naziv = "Bela Tehnika" });
            kategorijas.Add(new Kategorija { Id = 2, Naziv = "Bastenska Tehnika" });

            var mockRepository = new Mock<IKategorijaRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(kategorijas.AsEnumerable());
            var controller = new KategorijeController(mockRepository.Object);

            // Act
            IEnumerable<Kategorija> result = controller.GetAll();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(kategorijas.Count, result.ToList().Count);
            Assert.AreEqual(kategorijas.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(kategorijas.ElementAt(1), result.ElementAt(1));
        }

        [TestMethod]
        public void DeleteReturnsNotFound()
        {
            // Arrange 
            var mockRepository = new Mock<IKategorijaRepository>();
            var controller = new KategorijeController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void DeleteReturnsOk()
        {
            // Arrange
            var mockRepository = new Mock<IKategorijaRepository>();
            mockRepository.Setup(x => x.GetById(10)).Returns(new Kategorija { Id = 10 });
            var controller = new KategorijeController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRepository = new Mock<IKategorijaRepository>();
            var controller = new KategorijeController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Put(new Kategorija { Naziv = "sdfsd" }, 10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        [TestMethod]
        public void PostMethodSetsLocationHeader()
        {
            // Arrange
            var mockRepository = new Mock<IKategorijaRepository>();
            var controller = new KategorijeController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Post(new Kategorija { Id = 10, Naziv = "asdsd" });
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<Kategorija>;

            // Assert
            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi", createdResult.RouteName);
            Assert.AreEqual(10, createdResult.RouteValues["id"]);
        }




    }
}
