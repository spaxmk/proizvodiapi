﻿using ProizvodiAPI.Interfaces;
using ProizvodiAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProizvodiAPI.Controllers
{
    public class KategorijeController : ApiController
    {
        private IKategorijaRepository _repository { get; set; }

        public KategorijeController(IKategorijaRepository repository)
        {
            _repository = repository;
        }


        //GET api/kategorije/
        public IEnumerable<Kategorija> GetAll()
        {
            return _repository.GetAll();
        }

        //GET api/kategorije/1
        public IHttpActionResult GetById(int Id)
        {
            var kategorija = _repository.GetById(Id);
            if (kategorija == null)
            {
                return NotFound();
            }
            return Ok(kategorija);
        }

        //POST api/kategorije/
        public IHttpActionResult Post(Kategorija kategorija)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            _repository.Add(kategorija);
            return CreatedAtRoute("DefaultApi", new { Id = kategorija.Id }, kategorija);
        }

        public IHttpActionResult Put(Kategorija kategorija, int Id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (Id != kategorija.Id)
            {
                return BadRequest();
            }
            try
            {
                _repository.Update(kategorija);
            }
            catch (Exception)
            {
                return BadRequest();
            }
            return Ok();
            
        }

        public IHttpActionResult Delete(int id)
        {
            var kategorija = _repository.GetById(id);
            if (kategorija == null)
            {
                return NotFound();
            }
            _repository.Delete(kategorija);
            return Ok();
        }
    }
}
