﻿using ProizvodiAPI.Interfaces;
using ProizvodiAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ProizvodiAPI.Controllers
{
    public class ProizvodiController : ApiController
    {
        private IProizvodRepository _repository { get; set; }
        public ProizvodiController(IProizvodRepository repository)
        {
            _repository = repository; ;
        }

        [ResponseType(typeof(Proizvod))]
        public IEnumerable<Proizvod> GetAll()
        {
            return _repository.GetAll();
        }

        [ResponseType(typeof(Proizvod))]
        public IHttpActionResult GetById(int Id)
        {
            var proizvod = _repository.GetById(Id);
            if (proizvod == null)
            {
                return NotFound();
            }
            return Ok(proizvod);
        }

        [ResponseType(typeof(Proizvod))]
        public IHttpActionResult Post(Proizvod proizvod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            _repository.Add(proizvod);
            return CreatedAtRoute("DefaultApi", new { Id = proizvod.Id }, proizvod);
        }

        [ResponseType(typeof(Proizvod))]
        public IHttpActionResult Put(Proizvod proizvod, int Id)
        {
            
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            if (Id != proizvod.Id)
            {
                return BadRequest();
            }
            try
            {
                _repository.Update(proizvod);
                
            }
            catch (Exception)
            {
                return BadRequest();
            }
            return Ok();
        }

        [ResponseType(typeof(Proizvod))]
        public IHttpActionResult Delete(int Id)
        {
            var proizvod = _repository.GetById(Id);
            if(proizvod == null)
            {
                return NotFound();
            }
            _repository.Delete(proizvod);
            return Ok();
        }

        [ResponseType(typeof(Proizvod))]
        public IEnumerable<Proizvod> GetFilter(int x)
        {
            return _repository.GetFilter(x);
        }
    }
}
