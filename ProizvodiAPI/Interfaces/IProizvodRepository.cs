﻿using ProizvodiAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProizvodiAPI.Interfaces
{
    public interface IProizvodRepository
    {
        IEnumerable<Proizvod> GetAll();
        IEnumerable<Proizvod> GetFilter(int x);
        Proizvod GetById(int Id);
        void Add(Proizvod proizvod);
        void Update(Proizvod proizvod);
        void Delete(Proizvod proizvod);
    }
}
