﻿using ProizvodiAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProizvodiAPI.Interfaces
{
    public interface IKategorijaRepository
    {
        IEnumerable<Kategorija> GetAll();
        Kategorija GetById(int Id);
        void Add(Kategorija kategorija);
        void Update(Kategorija kategorija);
        void Delete(Kategorija kategorija);
    }
}
