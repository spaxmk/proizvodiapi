namespace ProizvodiAPI.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProizvodiAPI.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProizvodiAPI.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            context.Kategorije.AddOrUpdate(
                new Models.Kategorija() { Id = 1, Naziv = "Odeca"},
                new Models.Kategorija() { Id = 2, Naziv = "Obuca"}
                );
            context.SaveChanges();

            context.Proizvodi.AddOrUpdate(
                new Models.Proizvod() { Id = 1, Naziv = "Majica", Cena = 899.9M, KategorijaId = 1 },
                new Models.Proizvod() { Id = 2, Naziv = "Trenerka", Cena = 989.65M, KategorijaId = 1 },
                new Models.Proizvod() { Id = 3, Naziv = "Kacket", Cena = 199.99M, KategorijaId = 1 },
                new Models.Proizvod() { Id = 4, Naziv = "Farmerke", Cena = 789.99M, KategorijaId = 1 },
                new Models.Proizvod() { Id = 5, Naziv = "Carape", Cena = 99.99M, KategorijaId = 1 },
                new Models.Proizvod() { Id = 6, Naziv = "Papuce", Cena = 249.99M, KategorijaId = 2 },
                new Models.Proizvod() { Id = 7, Naziv = "Patike", Cena = 799.99M, KategorijaId = 2 },
                new Models.Proizvod() { Id = 8, Naziv = "Cipele", Cena = 699.99M, KategorijaId = 2 },
                new Models.Proizvod() { Id = 3, Naziv = "Stikle", Cena = 399.99M, KategorijaId = 2 }
                );
            context.SaveChanges();

        }
    }
}
