﻿using ProizvodiAPI.Interfaces;
using ProizvodiAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace ProizvodiAPI.Repository
{
    public class ProizvodRepository : IDisposable, IProizvodRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void Add(Proizvod proizvod)
        {
            db.Proizvodi.Add(proizvod);
            db.SaveChanges();
        }

        public void Delete(Proizvod proizvod)
        {
            db.Proizvodi.Remove(proizvod);
            db.SaveChanges();
        }

        public IEnumerable<Proizvod> GetAll()
        {
            return db.Proizvodi;
        }

        public Proizvod GetById(int Id)
        {
            return db.Proizvodi.FirstOrDefault(p => p.Id == Id);
        }

        public void Update(Proizvod proizvod)
        {
            db.Entry(proizvod).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }
        }

        public IEnumerable<Proizvod> GetFilter(int x)
        {
            IEnumerable<Proizvod> query = from p in db.Proizvodi.Include(k => k.Kategorija).OrderBy(p => p.Cena)
                                          where p.Cena < x
                                          select p;
            return query;
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}