﻿using ProizvodiAPI.Interfaces;
using ProizvodiAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace ProizvodiAPI.Repository
{
    public class KategorijaRepository : IDisposable, IKategorijaRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext(); 

        public void Add(Kategorija kategorija)
        {
            db.Kategorije.Add(kategorija);
            db.SaveChanges();
        }

        public void Delete(Kategorija kategorija)
        {
            db.Kategorije.Remove(kategorija);
            db.SaveChanges();
        }

        public IEnumerable<Kategorija> GetAll()
        {
            return db.Kategorije;
        }

        public Kategorija GetById(int Id)
        {
            return db.Kategorije.FirstOrDefault(k => k.Id == Id);
        }

        public void Update(Kategorija kategorija)
        {
            db.Entry(kategorija).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }


        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
